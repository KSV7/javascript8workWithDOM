function geText(l) {
    var result = '';
    var characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    var charactersLength = characters.length;

    for ( var i = 0; i < l; i++ ) {
        result += characters.charAt(Math.floor(Math.random() * charactersLength));
    }

    return result;
}

const createNewChild = () => {
    const list = document.querySelector("#listdiv");
    
    if (list.childNodes.length > 9) {
        removeAllChild();
    } else {
        // создание элемента div
        const item = document.createElement("div");
        item.innerHTML = geText(5);
        // добавление созданного элемента
        list.appendChild(item);
    }
}

const removeAllChild = () => {
    const list = document.querySelector("#listdiv");
    if (list != null) {
        // удаление всех элементов в диве.
        list.innerHTML = '';
    }
}